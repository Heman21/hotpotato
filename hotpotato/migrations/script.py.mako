"""
${message}

Revision ID: ${up_revision}
Revises: ${down_revision | comma,n}
Create Date: ${create_date}
"""


import sqlalchemy as sa
from alembic import context, op
${imports if imports else ""}

# revision identifiers, used by Alembic.
revision = ${repr(up_revision)}
down_revision = ${repr(down_revision)}
branch_labels = ${repr(branch_labels)}
depends_on = ${repr(depends_on)}

num_insert_ops = 1


def database_insert(table, data, row=None):
    """
    Helper function for adding data to a table.

    Give it the table and a mutable data list, and it will insert rows in a controlled
    manner, by splitting the insert operation into smaller ones with a maximum 100 rows.

    Insert data into it by passing it to the row keyword argument, as a dictionary.

    Once all the data has been run through this function, call it one more time
    without the row parameter to flush the rest of tha data.
    """

    global num_insert_ops

    if not row or len(data) >= 100:
        print("{} INSERT {}".format(num_insert_ops, len(data)))
        op.bulk_insert(table, data)
        data.clear()
        num_insert_ops += 1

    if row:
        data.append(row)


def row_select(session, table, *columns, **search_params):
    """
    Return the results of a SELECT query.

    Returns a generator of single elements if there was only one column specified,
    otherwise returns a generator of arrays of elements.
    """

    cols = [table.columns[col] for col in columns]

    query = session.query(*cols)

    if search_params:
        for key, value in search_params.items():
            query = query.filter(table.columns[key] == value)

    return (data[0] for data in query) if len(columns) == 1 else query


def row_update(table, row_id, **columns):
    """
    Update a row on the given table with the given column values.
    """

    context.execute(table.update().where(table.c.id == row_id).values(**columns))


def upgrade():
    """
    Upgrade the database.
    """

    print("Performing upgrade migration...")

    with context.begin_transaction():
        schema_upgrade()
    data_upgrade()

    print("Upgrade migration complete.")


def downgrade():
    """
    Downgrade the database to the previous version.
    """

    print("Performing downgrade migration...")

    data_downgrade()
    with context.begin_transaction():
        schema_downgrade()

    print("Downgrade migration complete.")


def schema_upgrade():
    """
    Upgrade the database schema.
    """

    print("Performing schema upgrade...")

    # NOTE: Add print statements for each statement.
    # print("Adding column 'column_name'...")
    ${upgrades}

    print("Schema upgrade complete.")


def schema_downgrade():
    """
    Downgrade the database schema to the previous version.
    """

    print("Performing schema downgrade...")

    # NOTE: Add print statements for each statement.
    # print("Dropping column 'column_name'...")
    ${downgrades}

    print("Schema downgrade complete.")


def data_upgrade():
    """
    Upgrade the database data.
    """

    pass
    # with context.begin_transaction():
    #     print("Performing data upgrade...")
    #     bind = op.get_bind()
    #     session = sa.orm.Session(bind=bind)
    #     table_name = sa.Table("table_name", ...)
    #     ...
    #     print("Data upgrade complete.")


def data_downgrade():
    """
    Downgrade the database data to the previous version.
    """

    pass
    # with context.begin_transaction():
    #     print("Performing data downgrade...")
    #     bind = op.get_bind()
    #     session = sa.orm.Session(bind=bind)
    #     table_name = sa.Table("table_name", ...)
    #     ...
    #     print("Data upgrade complete.")
