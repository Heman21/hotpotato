"""
Notification pager sender base class.
"""


import flask

from hotpotato.notifications.senders.base import BaseSender


# pylint: disable=abstract-method
class PagerBaseSender(BaseSender):
    """
    Notification pager base sender.
    """

    method = "pager"

    def can_send(self, user_id):
        """
        Return True if the user with the given ID can use the pager sender,
        and sending via pager is enabled.
        """

        return (
            super().can_send(user_id)
            if flask.current_app.config["PAGER_ENABLED"]
            else False
        )
