"""
Notification sender constants and helper classes.
"""


import types

from hotpotato.app.main import app
from hotpotato.notifications.senders import (
    pager_modica,
    pushover,
    sms_modica,
    sms_twilio,
)
from hotpotato.notifications.senders.debug_sender import DebugSender

APP = tuple()
# APP = ( # TODO: Hot Potato app
#    app.AppSender(),
# )

PUSHOVER = tuple((pushover.PushoverSender(),))

PAGER = tuple((pager_modica.ModicaPagerSender(),))

SMS = tuple((sms_modica.ModicaSMSSender(), sms_twilio.TwilioSMSSender()))

ALL = APP + PUSHOVER + PAGER + SMS

if app.config["TESTING"]:
    FOR_METHOD = types.MappingProxyType(
        {
            "app": tuple((DebugSender("app"),)),
            "pushover": tuple((DebugSender("pushover"),)),
            "pager": tuple((DebugSender("pager"),)),
            "sms": tuple((DebugSender("sms"),)),
        }
    )
else:
    FOR_METHOD = types.MappingProxyType(
        {"app": APP, "pushover": PUSHOVER, "pager": PAGER, "sms": SMS}
    )
