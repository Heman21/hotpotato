"""
Notification classes and helper functions.
"""


import json
from datetime import datetime

import sqlalchemy

from hotpotato import util
from hotpotato.models import Notification as Model, db

from hotpotato.notifications import (  # noqa: F401; Import package globals so they can be referenced from this module by external code.
    APP_PROVIDER,
    JSON_VERSION,
    METHOD,
    PAGER_PROVIDER,
    PROVIDER,
    PROVIDER_FOR_METHOD,
    SMS_PROVIDER,
    STATUS,
    TYPE,
    exceptions,
)


# Not actually a valid notification type, but is used by API notification handlers
# to help them define special behaviour for when representing things as generic notifications
# are required.
NOTIF_TYPE = "notification"


JSON_API_PARAMS = frozenset(
    (
        "version",
        "node_name",
        "event_id",
        "ticket_id",
        "warnings",
        "errors",
        "status",
        "method",
        "provider",
        "provider_notif_id",
    )
)


def get(notif_id):
    """
    Get a notification.
    """

    obj = Model.query.filter_by(id=notif_id).first()

    if not obj:
        raise exceptions.NotificationIDError(notif_id)

    return obj


def get_by(**kwargs):
    """
    Return a generator of all notifications matching the given search parameters.
    """

    return (obj for obj in get_by_helper(**kwargs))


def get_by_provider_notif_id(provider, provider_notif_id):
    """
    Get a notification by its handling provider and the corresponding notification ID.
    """

    if provider not in PROVIDER:
        raise exceptions.NotificationProviderError(provider)

    obj = Model.query.filter(
        in_json("provider", provider), in_json("provider_notif_id", provider_notif_id)
    ).first()

    if not obj:
        raise exceptions.NotificationProviderNotificationIDError(
            provider, provider_notif_id
        )

    return obj


def get_query(notif_id=None):
    """
    Get a query object for selecting the row object for a notification.
    If notif_id is specified, applies a filter for it on the query object.
    """

    if notif_id:
        return Model.query.filter_by(id=notif_id)
    else:
        return Model.query


def in_json(key, value):
    """
    Return an SQLAlchemy expression object for checking if the given value is
    available at the given key in the JSON field of a notification.
    Used in query filters.
    """

    return Model.json.op("@>")(json.dumps({key: value}))


def not_in_json(key, value):
    """
    Return an SQLAlchemy expression object for checking if the given value is
    *NOT* available at the given key in the JSON field of a notification.
    Used in query filters.
    """

    return sqlalchemy.not_(in_json(key, value))


# pylint: disable=too-many-arguments
def create(tenant_id, notif_model, body, user_id=None, status=None, json_obj=None):
    """
    Create a new notification.
    """

    received_dt = datetime.utcnow()
    if not status:
        status = "NEW" if user_id else "UNSENT"

    # if notif_type not in TYPE:
    #     raise exceptions.NotificationTypeError(notif_type)
    if status not in STATUS:
        raise exceptions.NotificationStatusError(status)

    # Add the API standard data to the JSON object.
    if not json_obj:
        json_obj = {}
    json_obj.update(
        {
            "version": JSON_VERSION,
            "node_name": util.node_name,
            "event_id": None,  # TODO: automatically fill event_id in?
            "ticket_id": None,  # TODO: automatically fill ticket_id in?
            "warnings": [],
            "errors": [],
            "status": status,
            "method": None,
            "provider": None,
            "provider_notif_id": None,
        }
    )

    obj = notif_model(
        tenant_id=tenant_id,
        user_id=user_id,
        received_dt=received_dt,
        body=body,
        json=json_obj,
    )

    db.session.add(obj)
    db.session.commit()

    return obj


#
# Internal helper methods.
#


def get_by_helper(**kwargs):
    """
    Return a query result object of all notifications matching the given search parameters.
    """

    model_keys = tuple(column.key for column in Model.__table__.columns)

    query = Model.query

    for key, value in kwargs.items():
        if key == "json":
            continue
        elif key in model_keys:
            query = query.filter(getattr(Model, key) == value)
        else:
            query = query.filter(in_json(key, value))

    return query.all()
