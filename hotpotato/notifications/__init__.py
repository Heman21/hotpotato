"""
Notification constants and helper classes.
"""


import types

JSON_VERSION = 1


# A frozen set of values representing the type of a notification.
# NOTE: This list referenced by the Hot Potato database. If it is changed,
#       a new database migration will need to be created.
TYPE = frozenset(("alert", "handover", "message"))


# A frozen set of values representing the status of a notification.
STATUS = frozenset(
    (
        "NEW",
        "UNSENT",
        "SEND_FAILED",
        "SEND_FAILED_ACKNOWLEDGED",
        "SENDING",
        "SENT_TO_PROVIDER",
        "RECEIVED_BY_PROVIDER",
        "SENT_TO_CLIENT",
        "RECEIVED_BY_CLIENT",
        "READ_BY_CLIENT",
    )
)


# A frozen set of values representing verifiable sending methods.
VERIFIABLE_METHOD = frozenset(("app", "pushover"))

# A frozen set of values representing unverifiable sending methods.
UNVERIFIABLE_METHOD = frozenset(("pager", "sms"))

# A frozen set of values representing all possible sending methods for a notification.
METHOD = VERIFIABLE_METHOD | UNVERIFIABLE_METHOD


# A frozen set of values representing app notification sending providers.
APP_PROVIDER = frozenset()
# APP_PROVIDER = frozenset((
#    "app",
# ))

# A frozen set of values representing pager notification sending providers.
PAGER_PROVIDER = frozenset(("modica",))

# A frozen set of values representing SMS notification sending providers.
SMS_PROVIDER = frozenset(("modica", "twilio"))

# A frozen set of values representing Pushover notification sending providers.
PUSHOVER_PROVIDER = frozenset(("pushover",))

# A frozen set of values representing all possible sending providers for a notification.
PROVIDER = APP_PROVIDER | PAGER_PROVIDER | SMS_PROVIDER | PUSHOVER_PROVIDER

# A MappingProxyType (immutable rendering of a dict) object providing a mapping
# of possible sending providers for each method.
PROVIDER_FOR_METHOD = types.MappingProxyType(
    {
        "sms": SMS_PROVIDER,
        "pager": PAGER_PROVIDER,
        "app": APP_PROVIDER,
        "pushover": PUSHOVER_PROVIDER,
    }
)
