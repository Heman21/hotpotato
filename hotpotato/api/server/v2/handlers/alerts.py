"""
Web API version 1 alert handler.
"""


from marshmallow import fields

from hotpotato.api.server.v2.handlers import notifications as handler_notifications
from hotpotato.notifications.alerts import NOTIF_TYPE, get

__all__ = ["NOTIF_TYPE", "get", "schema"]


class AlertSchema(handler_notifications.NotificationSchema):
    alert_type = fields.Str(required=True, json=True)
    server_id = fields.Int(required=True, json=True)
    trouble_code = fields.Str(required=True, json=True)
    hostname = fields.Str(required=True, json=True)
    display_name = fields.Str(required=True, json=True)
    service_name = fields.Str(required=True, json=True)
    state = fields.Str(required=True, json=True)
    output = fields.Str(required=True, json=True)
    timestamp = fields.Str(required=True, format="iso", json=True)


schema = AlertSchema()
