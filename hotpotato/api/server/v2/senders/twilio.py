"""
Server API version 2 endpoints for Modica callback functions.
"""


from hotpotato.api.server import twilio
from hotpotato.api.server.v2._blueprint import blueprint

# TODO: Handle incoming messages in Twilio
#       https://www.twilio.com/docs/sms/tutorials/how-to-receive-and-reply-python


@blueprint.route("/senders/twilio/messagestatus", methods=["POST"])
def senders_twilio_messagestatus():
    """
    Handle delivery status receipts from Twilio.
    """

    return twilio.notificationstatus()
