"""
Server API version 2 helper functions.
"""


import copy
import re
from http import HTTPStatus

import flask

from hotpotato import servers
from hotpotato.api import functions as api_functions
from hotpotato.api.server.v2 import exceptions as api_server_exceptions


def server_get_from_auth():
    """
    Find the object representing the server making the request using its auth info,
    and return it.
    """

    headers = flask.request.headers

    if "Authorization" not in headers:
        raise api_server_exceptions.APIServerV2AuthError(
            "No Authorization header provided",
            success=False,
            code=HTTPStatus.UNAUTHORIZED,
        )

    result = re.match("^([A-Za-z]*) (.*)$", headers.get("Authorization"))

    if result is None:
        raise api_server_exceptions.APIServerV2AuthError(
            "Authorization header provided is invalid",
            success=False,
            code=HTTPStatus.UNAUTHORIZED,
        )

    method = result.group(1).lower()
    if method != "apikey":
        raise api_server_exceptions.APIServerV2AuthError(
            "Unknown Authorization method '{}', expected 'apikey'".format(method),
            success=False,
            code=HTTPStatus.UNAUTHORIZED,
        )

    api_key = result.group(2)
    try:
        return servers.get_by_api_key(api_key)
    except servers.ServerAPIKeyError:
        raise api_server_exceptions.APIServerV2AuthError(
            "Invalid API key '{}'".format(api_key),
            success=False,
            code=HTTPStatus.UNAUTHORIZED,
        )


def api_json_response_get(
    message=None, data=None, success=True, code=HTTPStatus.OK, no_cache=False
):
    """
    Return a Server API version 2 standard JSON response.
    """

    if not isinstance(success, bool):
        raise api_server_exceptions.APIServerV2ResponseError(
            "Invalid value for success: got {}, expected bool".format(
                type(success).__name__
            ),
            success=False,
            code=HTTPStatus.INTERNAL_SERVER_ERROR,
        )
    if not isinstance(code, int):
        raise api_server_exceptions.APIServerV2ResponseError(
            "Invalid value for code: got {}, expected int".format(type(code).__name__),
            success=False,
            code=HTTPStatus.INTERNAL_SERVER_ERROR,
        )
    if message and not isinstance(message, str):
        raise api_server_exceptions.APIServerV2ResponseError(
            "Invalid value for message: got {}, expected str".format(
                type(message).__name__
            ),
            success=False,
            code=HTTPStatus.INTERNAL_SERVER_ERROR,
        )
    if data and not isinstance(data, dict):
        raise api_server_exceptions.APIServerV2ResponseError(
            "Invalid value for data: got {}, expected dict".format(type(data).__name__),
            success=False,
            code=HTTPStatus.INTERNAL_SERVER_ERROR,
        )

    return api_functions.json_response_get(
        {
            "success": success,
            "code": code,
            "message": message,
            "data": copy.deepcopy(data) if data else {},
        },
        code=code,
        no_cache=no_cache,
    )
