"""
Server API version 2 exception classes.
"""


from hotpotato.api import exceptions as api_exceptions


class APIServerV2Error(api_exceptions.APIError):
    """
    Server API version 2 exception base class.
    """

    pass


class APIServerV2HandlerError(APIServerV2Error):
    """
    Server API version 2 notification handler exception.
    """

    pass


class APIServerV2AuthError(APIServerV2Error):
    """
    Server API version 2 authentication exception.
    """

    pass


class APIServerV2ResponseError(APIServerV2Error):
    """
    Server API version 2 response exception.
    """

    pass
