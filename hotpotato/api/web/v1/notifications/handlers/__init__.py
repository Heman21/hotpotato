"""
Web API version 1 notification handler functions.
"""


from http import HTTPStatus

from hotpotato.api.web.v1.notifications import (
    exceptions as api_web_notifications_exceptions,
)
from hotpotato.api.web.v1.notifications.handlers import (
    alerts as handler_alerts,
    handovers as handler_handovers,
    messages as handler_messages,
    notifications as handler_notifications,
)

_notif_handlers = {
    "notifications": handler_notifications,
    "alerts": handler_alerts,
    "handovers": handler_handovers,
    "messages": handler_messages,
}


def get(handler_name):
    """
    Return the notification handler for the given handler name.
    """

    if handler_name not in _notif_handlers:
        raise api_web_notifications_exceptions.APIWebV1NotificationHandlerError(
            "Invalid notification handler type: {}".format(handler_name),
            success=False,
            code=HTTPStatus.BAD_REQUEST,
        )

    return _notif_handlers[handler_name]
