"""
API notification functions.
"""


from datetime import datetime
from http import HTTPStatus

import sqlalchemy
import werkzeug.datastructures

from hotpotato import util
from hotpotato.api import functions as api_functions
from hotpotato.api.functions import exceptions as api_functions_exceptions
from hotpotato.models import db
from hotpotato.notifications import notifications


def get(handler, notif_id):
    """
    Return the notification object with the given ID, using the given handler.
    """

    return handler.get(notif_id)


def as_dict(handler, notif):
    """
    Return a notification object as an API-compatible dictionary.
    """

    return handler.schema.dump(notif).data


def as_csv(handler, notifs):
    """
    Return a list of notifications in CSV format.
    """

    schema = handler.schema

    columns = tuple(schema.fields.keys())

    rows = tuple((tuple(schema.dump(notif).data.values()) for notif in notifs))

    return (columns, rows)


def search_filters_get(handler=None, ignore_notif_type=None, fail_if_empty=True):
    """
    Return a MultiDict of all notification search filters defined in the URL arguments.
    More than one value can be defined for a given key.
    """

    # If handler is specified, redefine ignore_notif_type according to
    # the type of notification the handler processes.
    if ignore_notif_type is None and handler:
        ignore_notif_type = handler.NOTIF_TYPE != "notification"

    try:
        sfs = api_functions.data_get().copy()
    except api_functions_exceptions.APIDataValueError:
        if fail_if_empty:
            raise api_functions_exceptions.APISearchFilterError(
                "No search filters specified",
                success=False,
                code=HTTPStatus.BAD_REQUEST,
            )
        return werkzeug.datastructures.ImmutableMultiDict()

    # Used when type is determined separately from search filters.
    if ignore_notif_type:
        sfs.poplist("notif_type")
        sfs.poplist("no_notif_type")

    return werkzeug.datastructures.ImmutableMultiDict(sfs.items(multi=True))


def get_from_search_filters(handler, search_filters, eager=False):
    """
    Take in a MultiDict representing search filters, and return a
    list of notification database objects that match.
    """

    # TODO: Issue #24 - replace this with a get_query() method from the specific module.
    query = notifications.get_query()
    if handler.NOTIF_TYPE != "notification":
        query = query.filter(notifications.Model.notif_type == handler.NOTIF_TYPE)

    # Filter to all notifications from the specified date.
    # NOTE: Time processing is required here, to shift the 24 hour period
    # specified by the user to its equivalent in UTC.
    start_date_dt = None
    if "start_date" in search_filters and search_filters["start_date"]:
        start_date_dt = util.datetime_process(
            datetime.strptime(search_filters["start_date"], "%Y-%m-%d"),
            as_user_tz=True,
            to_utc=True,
            naive=True,
        )
        query = query.filter(start_date_dt <= notifications.Model.received_dt)

    # Filter to all notifications up until the specified date.
    # NOTE: Time processing is required here, to shift the 24 hour period
    # specified by the user to its equivalent in UTC.
    end_date_dt = None
    if "end_date" in search_filters and search_filters["end_date"]:
        end_date_dt = util.datetime_process(
            datetime.strptime(search_filters["end_date"], "%Y-%m-%d"),
            as_user_tz=True,
            to_utc=True,
            naive=True,
        )
        query = query.filter(notifications.Model.received_dt < end_date_dt)

    # Sanity checks for the start_date and end_date parameters.
    if (start_date_dt or end_date_dt) and "received_dt" in search_filters:
        raise api_functions_exceptions.APISearchFilterError(
            "Search filter parameters 'received_dt' and 'start_date/end_date' "
            "are mutually exclusive",
            success=False,
            code=HTTPStatus.BAD_REQUEST,
        )
    if start_date_dt and end_date_dt and start_date_dt >= end_date_dt:
        raise api_functions_exceptions.APISearchFilterError(
            "start_date ({}) is set to a later date than end_date ({})".format(
                start_date_dt.date().isoformat(), end_date_dt.date().isoformat()
            ),
            success=False,
            code=HTTPStatus.BAD_REQUEST,
        )

    # Direct notification attribute filtering.
    # N.B. supports more than one value for each parameter, using the MultiDict structure.
    for param, values in search_filters.lists():
        if not values:
            continue
        if param in ("start_date", "end_date"):
            continue
        if handler.NOTIF_TYPE != "notification" and param in (
            "notif_type",
            "no_notif_type",
        ):
            continue

        if param.startswith("not_"):
            search_equals = False
            raw_param = param[4:]
            if raw_param in search_filters:
                raise api_functions_exceptions.APISearchFilterError(
                    "Search filter parameter '{}': "
                    "equal and not-equal search filters cannot be combined".format(
                        raw_param
                    ),
                    success=False,
                    code=HTTPStatus.BAD_REQUEST,
                )
        else:
            search_equals = True
            raw_param = param
            if "not_{}".format(raw_param) in search_filters:
                raise api_functions_exceptions.APISearchFilterError(
                    "Search filter parameter '{}': "
                    "equal and not-equal search filters cannot be combined".format(
                        raw_param
                    ),
                    success=False,
                    code=HTTPStatus.BAD_REQUEST,
                )

        if raw_param not in handler.schema.fields:
            raise api_functions_exceptions.APISearchFilterError(
                "Unsupported search filter parameter '{}'".format(param),
                success=False,
                code=HTTPStatus.BAD_REQUEST,
            )

        field = handler.schema.fields[raw_param]

        # Iterate over all values, and store their respective filters.
        filters = []
        for value in values:

            if field.metadata.get("json", None):
                if search_equals:
                    filters.append(
                        notifications.in_json(raw_param, field.deserialize(value))
                    )
                else:
                    filters.append(
                        notifications.not_in_json(raw_param, field.deserialize(value))
                    )
            else:
                if search_equals:
                    filters.append(
                        notifications.Model.__table__.columns[raw_param]
                        == field.deserialize(value)
                    )
                else:
                    filters.append(
                        notifications.Model.__table__.columns[raw_param]
                        != field.deserialize(value)
                    )

        # Generate a simple filter in the case of a single value.
        if len(filters) == 1:
            query = query.filter(filters[0])
        # Generate a complex filter in the case of multiple values,
        # which tests if it is any one of the given values (boolean OR).
        elif len(filters) > 1:
            query = query.filter(sqlalchemy.or_(*filters))
        # Of course, do nothing for len(filters) < 1.

    if eager:
        query = query.options(db.joinedload(notifications.Model.user, innerjoin=True))

    return query.order_by(notifications.Model.received_dt.desc()).all()
