"""
functions.py contains all the functions used with Hot Potato
"""
import re
from datetime import datetime

import pytz
import requests
from flask import current_app
from twilio.rest import Client


def to_local_time(date, tz=None, format_="%Y-%m-%d %H:%M:%S"):
    """
    Formats a utc datetime into a user's timezone
    """
    if date.tzinfo is None:
        date = pytz.utc.localize(date)

    if (
        not tz
    ):  # Sometimes an empty string is passed, so this has to be here as an if statement
        tz = "UTC"

    local_date = date.astimezone(pytz.timezone(tz))
    return datetime.strftime(local_date, format_)


def sms_via_twilio(message_body, number):
    """
    Sends an SMS message via Twilio
    """
    account_sid = current_app.config["TWILIO_ACCOUNT_SID"]
    auth_token = current_app.config["TWILIO_AUTH_TOKEN"]
    sms_number = current_app.config["TWILIO_SMS_NUMBER"]

    client = Client(account_sid, auth_token)

    try:
        sms_message = client.messages.create(
            number, body=message_body, from_=sms_number
        )
        print(
            "SMS message [{0}] sent to {1} via Twilio with ID: {2}".format(
                message_body, number, sms_message.sid
            )
        )
    # pylint: disable=broad-except
    except Exception:
        print(
            "Failed to send SMS message [{0}] to {1} via Twilio".format(
                message_body, number
            )
        )


def sms_via_modica(message_body, destination_number):
    """
    Sends an SMS Message via the Modica API

    Works only for NZ Destination numbers
    """

    # Modica API Credentials
    api_url = current_app.config["MODICA_URL"]
    api_user = current_app.config["MODICA_USERNAME"]
    api_pass = current_app.config["MODICA_PASSWORD"]

    message_body = message_body[:160]  # SMS Length limit is 160 char

    # Check if the message starts with a +64 first
    if not destination_number.startswith("+64"):
        print(
            "Error: Not sending SMS message to {1} via Modica as the number doesn't start with a +64, trying Twilio"
        )
        sms_via_twilio(message_body, destination_number)

    else:
        # Send the message
        try:
            response = requests.post(
                "{}/{}".format(api_url, "messages"),
                auth=(api_user, api_pass),
                timeout=5,
                json={"destination": destination_number, "content": message_body},
            )

            if response.status_code == 201:
                # Remove the [] that Modica enclose the ID in.
                modicaid = re.sub(r"[\[\]]", "", response.text)
                print(
                    "SMS message submitted to Modica with ID: {0} at {1} (UTC)".format(
                        modicaid, datetime.utcnow()
                    )
                )
                return int(modicaid)

            else:
                print(
                    "Error: Did not get a 201 on submission to Modica's API. Failed to send SMS message '{1}' to {2} at {3} (UTC) with response {4} ({5})".format(
                        message_body,
                        destination_number,
                        datetime.utcnow(),
                        response.status_code,
                        response.text,
                    )
                )

        # pylint: disable=broad-except
        except Exception:
            print(
                "Error: Exception on submission to Modica's API. Failed to send SMS message '{1}' to {2} at {3} (UTC) with response {4} ({5})".format(
                    message_body,
                    destination_number,
                    datetime.utcnow(),
                    response.status_code,
                    response.text,
                )
            )

    return "ERROR"


def page_via_modica(message):
    """
    Sends a pager message via the Modica API
    """
    # Modica API Credentials
    api_url = current_app.config["MODICA_URL"]
    api_user = current_app.config["MODICA_USERNAME"]
    api_pass = current_app.config["MODICA_PASSWORD"]

    # Strip quotes out of the message
    message = re.sub("['\"\r]", "", message)

    # Replace all repeated whitespaces with a single space
    # This works by splitting the message into a list of strings then joining them again
    message = " ".join(message.split())

    # Make the pager messages one page
    singlemessage = message[:200]  # Was 220

    # Send the message to Modica
    try:
        response = requests.post(
            "{}/{}".format(api_url, "messages"),
            auth=(api_user, api_pass),
            timeout=5,
            json={
                "destination": current_app.config["MODICA_PAGER_NUMBER"],
                "content": singlemessage,
            },
        )

        # Evaluate the response
        if response.status_code == 201:
            # Remove the [] that Modica enclose the ID in.
            modicaid = re.sub(r"[\[\]]", "", response.text)
            print(
                "Pager message submitted to Modica with ID: {0} at {1} (UTC)".format(
                    modicaid, datetime.utcnow()
                )
            )
            return int(modicaid)

        else:
            print(
                "Error: Did not get 201 on submission to Modica's API. Failed to send message with {1} at {2} (UTC) with response: {3}".format(
                    response.status_code, datetime.utcnow(), response.text
                )
            )
            print("The message we were trying to send was: '{0}'".format(singlemessage))

    # pylint: disable=broad-except
    except Exception:
        print(
            "Exception when attempting to communicate with Modica's API at {0} (UTC) with a '{1}' status code and '{2}' for the body".format(
                datetime.utcnow(), response.status_code, response.text
            )
        )
        print("The message we were trying to send was: '{0}'".format(singlemessage))

    # Return and let the add_notification_v1 function handle the database side of things for the notification

    return "ERROR"
