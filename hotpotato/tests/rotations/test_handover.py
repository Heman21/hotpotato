"""
Test handover related views
"""

from hotpotato.tests import rotations, users
from hotpotato.tests.conftest import login


def test_index_no_one_on_pager(app, client, session):
    """
    Test that the index page loads without errors when no one is on pager.
    """
    user = users.UserFactory()
    rotations.RotationFactory(user=None)
    session.commit()
    with app.app_context(), login(client, user):
        r = client.get("/")
        assert r.status_code == 200


def test_handover_no_one_on_pager(app, client, session, make_user):
    """
    Test that a handover completes successfully when no one is currently on pager.
    """
    user = make_user("admin")
    rotations.RotationFactory(user=None)
    session.commit()
    with app.app_context(), login(client, user):
        r = client.post("/handover", data={"message": ""})
        assert r.status_code == 200
