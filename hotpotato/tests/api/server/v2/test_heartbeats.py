"""
Test the V2 heartbeat endpoint
"""

from http import HTTPStatus

from sqlalchemy import desc

from hotpotato.models import Heartbeat
from hotpotato.tests import servers


def test_create_heartbeat(client, session):
    """
    Test calling the create heartbeat endpoint creates a heartbeat and returns 201.
    """
    server = servers.ServerFactory()
    session.commit()

    r = client.post(
        "/api/server/v2/heartbeats/create",
        headers={"Authorization": "apikey {}".format(server.apikey)},
    )

    assert r.status_code == HTTPStatus.CREATED

    heartbeat = (
        session.query(Heartbeat)
        .filter(Heartbeat.server_id == server.id)
        .order_by(desc(Heartbeat.received_dt))
        .first()
    )

    assert heartbeat.id == r.json["data"]["id"]
