from datetime import datetime
from http import HTTPStatus

from hotpotato.tests import notifications, users
from hotpotato.tests.conftest import login


def test_get_notifications_no_notifications(app, client, session):
    """
    Test that a request for a period with no notifications returns 200.
    """
    user = users.UserFactory()
    session.commit()
    with app.app_context(), login(client, user):
        r = client.get(
            "/api/web/v1/notifications/get?end_date=2018-11-27&start_date=2018-11-26"
        )

        assert r.status_code == HTTPStatus.OK
        assert r.json["success"] is True


def test_get_notifications_one_notification(app, client, session):
    """
    Test that a request for a period with one notification returns that one notification.
    """
    user = users.UserFactory(timezone="UTC")
    session.commit()
    with app.app_context(), login(client, user):
        alert = notifications.NotificationFactory(
            notif_type="alert", received_dt=datetime(2018, 11, 26, hour=11)
        )
        notifications.NotificationFactory(
            notif_type="alert", received_dt=datetime(2018, 11, 25, hour=11)
        )
        session.commit()
        print(alert.received_dt)
        r = client.get(
            "/api/web/v1/notifications/get?end_date=2018-11-27&start_date=2018-11-26"
        )

        assert r.status_code == HTTPStatus.OK
        assert r.json["success"] is True
        assert r.json["data"][str(alert.id)]["id"] == alert.id
        assert len(r.json["data"]) == 1


# TODO: Test validity of CSV


def test_get_notifications_as_csv_no_notifications(app, client, session):
    """
    Test that getting the notifications as a CSV returns 200.
    """
    user = users.UserFactory()
    session.commit()
    with app.app_context(), login(client, user):
        r = client.get(
            "/api/web/v1/notifications/get/as_csv?end_date=2018-11-27&start_date=2018-11-26"
        )

        print(r.get_data(as_text=True))
        assert r.status_code == HTTPStatus.OK
