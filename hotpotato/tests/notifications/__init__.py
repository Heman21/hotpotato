"""
Notification unit test functions.
"""


import string

import factory
import faker

from hotpotato import models, util as hotpotato_util
from hotpotato.models import db
from hotpotato.notifications import alerts, notifications
from hotpotato.tests import servers, users, util


class NotificationFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.Notification
        sqlalchemy_session = db.session

    tenant_id = 0  # TODO: tenants
    user = factory.SubFactory(users.UserFactory)
    received_dt = factory.Faker("date_time_this_year")
    notif_type = factory.Faker("random_element", elements=notifications.TYPE)
    body = factory.Faker("text", max_nb_chars=160)

    @factory.lazy_attribute
    def json(self):
        return generate_json(faker.Faker(), self.notif_type, self.received_dt)[1]


def generate_json(fake, notif_type, received_dt):
    status = fake.random_element(notifications.STATUS)
    if status not in ["NEW", "UNSENT", "SEND_FAILED"]:
        # TODO: make it 'notifications.METHOD' when an app method gets introduced
        method = fake.random_element(["sms", "pager"])
        if method == "sms":
            provider = fake.random_element(notifications.SMS_PROVIDER)
        elif method == "pager":
            provider = fake.random_element(notifications.PAGER_PROVIDER)
        else:
            raise RuntimeError(
                "Invalid method value in notification test data: {}".format(method)
            )
        if provider == "modica":
            provider_notif_id = "".join(
                fake.random_elements(tuple(string.digits), length=10)
            )
        elif provider == "twilio":
            provider_notif_id = "SM" + "".join(
                fake.random_elements(tuple(string.digits), length=32)
            )
        else:
            raise RuntimeError(
                "Invalid provider value in message test data: {}".format(provider)
            )
    else:
        method = None
        provider = None
        provider_notif_id = None

    json_obj = {
        "version": notifications.JSON_VERSION,
        "node_name": hotpotato_util.node_name,
        "event_id": None,  # TODO: events
        "ticket_id": None,  # TODO: tickets
        "warnings": [],
        "errors": [],
        "status": status,
        "method": method,
        "provider": provider,
        "provider_notif_id": provider_notif_id,
    }
    if notif_type == "message":
        user_id, body = generate_message_json(fake, json_obj)
    elif notif_type == "handover":
        user_id, body = generate_handover_json(fake, json_obj)
    elif notif_type == "alert":
        user_id, body = generate_alert_json(fake, json_obj, received_dt)
    return (user_id, json_obj, body)


def generate_message_json(fake, json_obj):
    user_id = fake.random_element(
        [None, util.get_random_object(models.db.session, models.User).id]
    )
    from_user = fake.random_element(
        [None, util.get_random_object(models.db.session, models.User)]
    )
    if from_user:
        reply_to_notif_id = fake.random_element(
            [None, util.get_random_object(models.db.session, models.Notification).id]
        )
    else:
        reply_to_notif_id = None

    message = fake.text(max_nb_chars=160)
    if from_user:
        body = "Message from {} via Hot Potato: {}".format(from_user.name, message)
    else:
        body = "Message from Hot Potato: {}".format(message)

    json_obj.update(
        {
            "from_user_id": from_user.id if from_user else None,
            "reply_to_notif_id": reply_to_notif_id,
            "message": message,
        }
    )
    return user_id, body


def generate_handover_json(fake, json_obj):
    to_user = util.get_random_object(models.db.session, models.User)
    from_user = fake.random_element(
        [None, util.get_random_object(models.db.session, models.User)]
    )
    message = fake.random_element([None, fake.text(max_nb_chars=160)])

    # Used in the actual row generation operation below.
    if from_user:
        user_id = fake.random_element([None, from_user.id, to_user.id])
    else:
        user_id = fake.random_element([None, to_user.id])

    if from_user and message:
        body = "{} is now on call, taking over from {} with the following message: {}".format(
            to_user.name, from_user.name, message
        )
    elif from_user:
        body = "{} is now on call, taking over from {}.".format(
            to_user.name, from_user.name
        )
    elif message:
        body = "{} is now on call with the following message: {}".format(
            to_user.name, message
        )
    else:
        body = "{} is now on call.".format(to_user.name)

    json_obj.update(
        {
            "rotation_id": fake.pyint(),
            "to_user_id": to_user.id,
            "from_user_id": from_user.id if from_user else None,
            "message": message,
        }
    )
    return user_id, body


def generate_alert_json(fake, json_obj, received_dt):
    user_id = fake.random_element(  # noqa: F841
        [None, util.get_random_object(models.db.session, models.User).id]
    )
    alert_type = fake.random_element(alerts.TYPE)
    server = util.get_random_object(models.db.session, models.Server)
    if not server:
        server = servers.ServerFactory()
        db.session.commit()
    trouble_code = fake.word().upper() + "".join(
        fake.random_elements(tuple(string.digits), length=4)
    )
    hostname = "{}-prod-{}{}".format(
        fake.word().lower(), fake.word().lower(), fake.random_element([1, 2, 3])
    )
    output = fake.text(max_nb_chars=160)
    timestamp = hotpotato_util.datetime_get_as_string(received_dt)

    if alert_type == "service":
        service_name = fake.word()
        state = fake.random_element(alerts.SERVICE_STATE)
        body = (
            "{server_name}: {trouble_code} {hostname} {service_name} state is "
            "{state} ({output}) "
            "{timestamp}"
        ).format(
            server_name=server.hostname,
            trouble_code=trouble_code,
            hostname=hostname,
            service_name=service_name,
            state=state,
            output=output,
            timestamp=timestamp,
        )

    elif alert_type == "host":
        service_name = None
        state = fake.random_element(alerts.HOST_STATE)
        body = (
            "{server_name}: {trouble_code} {hostname} state is "
            "{state} ({output}) "
            "{timestamp}"
        ).format(
            server_name=server.hostname,
            trouble_code=trouble_code,
            hostname=hostname,
            state=state,
            output=output,
            timestamp=timestamp,
        )

    json_obj.update(
        {
            "alert_type": alert_type,
            "server_id": server.id,
            "trouble_code": trouble_code,
            "hostname": hostname,
            "display_name": hostname,
            "service_name": service_name,
            "state": state,
            "output": output,
            "timestamp": timestamp,
        }
    )
    return user_id, body


def create_fake(fake):
    """
    Generate a fake message, with randomly set data.
    """

    # pylint: disable=too-many-locals,too-many-branches,too-many-statements

    tenant_id = 0  # TODO: tenants
    received_dt = fake.date_time_this_year()
    notif_type = fake.random_element(notifications.TYPE)

    user_id, json_obj, body = generate_json(fake, notif_type, received_dt)

    # Create the notification_log database object, and return it.
    return models.Notification(
        tenant_id=tenant_id,
        user_id=user_id,
        received_dt=received_dt,
        notif_type=notif_type,
        body=body,
        json=json_obj,
    )
