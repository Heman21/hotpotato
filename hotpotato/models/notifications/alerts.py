from hotpotato import util
from hotpotato.models.database import db
from hotpotato.models.notifications.notifications import Notification


class Alert(Notification):
    """
    Alert object class.
    """

    name = "alert"
    description = "alert"

    # pylint: disable=arguments-differ
    def send(self, run_async=True, withhold_warnings=True, contact=None, method=None):
        """
        Send the Alert.
        """

        # The reason why this import is here and not at the top of the file is
        # because when the actors get defined, they connect to the message queue broker
        # to register themselves as jobs to be run.
        # Therefore, the message queue broker needs to already be running BEFORE
        # the actors get imported/defined.
        from hotpotato.notifications.queue.actors import alert as actor

        if self.has_been_sent():
            return

        # Do not actually send if it is just a warning, and withhold_warnings
        # is set to True.
        if withhold_warnings and self.json["state"] == "WARNING":
            self.json["status"] = "UNSENT"
            db.session.commit()
            return

        super().send(actor, run_async=run_async, contact=contact, method=method)

    def timestamp_get_as_datetime(self):
        """
        Return the timestamp value from this alert as a DateTime object.
        """

        return util.datetime_get_from_string(self.json["timestamp"])

    def timestamp_set_from_datetime(self, timestamp):
        """
        Set the timestamp value of this alert from the given DateTime object.
        """

        self.json["timestamp"] = util.datetime_get_as_string(timestamp)

    __mapper_args__ = {"polymorphic_identity": "alert"}
