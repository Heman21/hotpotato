"""
Command line interface (CLI) Hot Potato commands.
"""


import click
import flask

from hotpotato import __version__


@click.group("hotpotato", cls=flask.cli.AppGroup)
def hotpotato():
    """
    Hot Potato admin commands.
    """

    pass


@hotpotato.command("config")
def config():
    """
    Hot Potato configuration.
    """

    click.echo(flask.current_app.config)


@hotpotato.command("version")
def version():
    """
    Hot Potato version.
    """

    click.echo(__version__)
