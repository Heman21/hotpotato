"""
Command line interface to create database.
"""


import click
import flask
import flask.cli

from hotpotato import models


@click.group("database", cls=flask.cli.AppGroup)
def database():
    """
    Database commands.
    """

    pass


@database.command("create")
def create():
    """
    Create the database
    """
    models.create(flask.current_app)
    models.initialise(flask.current_app)
