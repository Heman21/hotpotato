"""
Helper exception classes.
"""


class ObjectValueError(Exception):
    """
    Object value exception base class.
    """

    pass


def object_value_error_message(
    value_description, actual_value, expected_value=None, possible_values=None
):
    """
    Return a formatted object value exception error message.
    """

    msg = "Invalid {}: ".format(value_description)

    if not expected_value and not possible_values:
        msg += "{}".format(actual_value)
    else:
        if expected_value:
            msg += "Expected {}, ".format(expected_value)
        else:
            msg += "Expected one of [{}], ".format(", ".join(possible_values))
        msg += "got {}".format(actual_value)

    return msg
