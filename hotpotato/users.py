"""
User functions.
"""


import flask_security

from hotpotato.models import db, user_datastore
from hotpotato.models.users import User as Model


class UserError(Exception):
    """
    User exception base class.
    """

    pass


class UserIDError(UserError):
    """
    Exception for invalid user ID.
    """

    def __init__(self, user_id):
        self.user_id = user_id
        super().__init__(
            "Invalid user ID (or unable to find user with ID): {}".format(user_id)
        )


class UserEmailError(UserError):
    """
    Exception for invalid user ID.
    """

    def __init__(self, email):
        self.email = email
        super().__init__(
            "Invalid user email (or unable to find user with email): {}".format(email)
        )


class UserTimezoneError(UserError):
    """
    Exception for invalid user timezone.
    """

    def __init__(self, timezone, email=None):
        self.email = email
        self.timezone = timezone
        if self.email:
            super().__init__(
                "Invalid timezone for user with email {}: {}".format(
                    self.email, self.timezone
                )
            )
        else:
            super().__init__("Invalid user timezone: {}".format(self.timezone))


def get(user_id):
    """
    Get a user object using the given ID, and return it.
    """

    obj = Model.query.filter_by(id=user_id).first()

    if not obj:
        raise UserIDError(user_id)

    return obj


def get_by_email(email):
    """
    Get a user object using the given email, and return it.
    """

    obj = Model.query.filter_by(email=email).first()

    if not obj:
        raise UserEmailError(email)

    return obj


def get_from_rotation(rotation):
    """
    Get the current on-call user from the given rotation.
    """

    return get(rotation.person)


def get_by(**kwargs):
    """
    Return a list of user objects which match the given search parameters.
    """

    return Model.query.filter_by(**kwargs).all()


def create(email, password, name, timezone, active=True):
    """
    Create a user object using the given input data, add it to the database,
    and return it.
    """

    hashed_password = flask_security.utils.hash_password(password)

    obj = user_datastore.create_user(
        email=email,
        password=hashed_password,
        name=name,
        timezone=timezone,
        active=active,
    )
    db.session.commit()

    return obj
