*** Settings ***
Documentation     Validate the Handover page.
Resource          resources/common.robot
Suite Setup       Open Browser To Login Page
Suite Teardown    Close Browser
Test Teardown     Logout


*** Test Cases ***
Perform Handover
    Login To User Not Currently On Pager
    Go To Handover Page
    Handover Page Should Be Open
    Input Message
    Take Over Pager
    Handover Page Should Be Open
    Pager Has Been Taken


*** Keywords ***
Login To User Not Currently On Pager
    ${test1_has_pager}=    Run Keyword And Return Status    Check If test1 Has Pager
    Run Keyword If    $test1_has_pager == False    Logout
    Run Keyword If    $test1_has_pager == False    Login To test1
    Run Keyword If    $test1_has_pager == True     Login To test2
    Home Page Should Be Open


Check If test1 Has Pager
    Login To test1
    Go To Handover Page
    Current User Has Pager
    Logout

Input Message
    Input Text    message    eu6tahngi7oo0chu6ohmopa4aecheyeKei9ohngul1eetuGh

Take Over Pager
    Click Button    Take Pager

Current User Has Pager
    Page Should Contain    You can't take the pager from yourself!

Pager Has Been Taken
    Page Should Contain    Pager taken!
