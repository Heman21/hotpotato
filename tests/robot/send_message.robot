*** Settings ***
Documentation     Validate the Send Message page and functionality.
Resource          resources/common.robot
Suite Setup       Open Browser To Login Page
Suite Teardown    Close Browser
Test Teardown     Logout

*** Test Cases ***
Send A Custom Message
    Login
    Go To Send Message Page
    Send Message Page Should Be Open
    Input Message
    Send Message
    Message Should Be Sent

*** Keywords ***
Input Message
    Input Text    message    uukie4naeCho0ohgh8yieghee2Choh0eema8aaf8raeh0iyi

Send Message
    Click Button    Send

Message Should Be Sent
    Page Should Contain    Message sent!
    Page Should Contain    Pager Message:
    Page Should Contain    uukie4naeCho0ohgh8yieghee2Choh0eema8aaf8raeh0iyi
