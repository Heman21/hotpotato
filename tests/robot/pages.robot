*** Settings ***
Documentation     Validate the Notifications page.
Resource          resources/common.robot
Suite Setup       Open Browser To Login Page
Suite Teardown    Close Browser
Test Teardown     Logout

*** Test Cases ***
Open Notifications Page
    Login
    Go To Notifications Page
    Notifications Page Should Be Open
